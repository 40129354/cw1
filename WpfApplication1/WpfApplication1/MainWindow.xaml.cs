﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    ///

    

    public  partial class MainWindow : Window
    
    {
        public Student student = new Student();
        public  MainWindow()
        {

            InitializeComponent();
            
        }
        
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            
        }

        private void TextBox_TextChanged_2(object sender, TextChangedEventArgs e)
        {

        }
        // Set Button
        private void Button_Click(object sender, RoutedEventArgs e)
        {   
            // Cheking if matriculation number is not blank
            if (matric.Text.Length > 0)
            {   // Convert matric.text to int and store it to mMatric
                int mMatric;
                bool isNumber = Int32.TryParse(matric.Text.Trim(), out mMatric);
                //Display an error if isNumber is false or out of range
                if (!isNumber || mMatric < 40000 || mMatric > 60000)
                {
                    MessageBox.Show("Matriculation Number must be between 40000 and 60000.");
                }
                else
                {
                    //Store the value
                    student.MatriculationNumber1 = mMatric;
                }
            }
            //Cheking if credits is not blank
            if (credits.Text.Length > 0)
            {
                // Convert credits.text to int and store it to mCredits
                int mCredits;
                bool isNumber = Int32.TryParse(credits.Text.Trim(), out mCredits);
                //Display an error if isNumber is false or out of range
                if (!isNumber || mCredits < 0 || mCredits > 480)
                {
                    MessageBox.Show("Credits must be between 0 and 480.");
                }
                else
                {
                    //Store the value
                    student.Credits1 = mCredits;
                }
            }
            //Checking if Date is not blank
            if (date.Text.Trim().Length > 0) 
            {
                //store the value
                student.DateOfBirth1 = date.Text.Trim();
            }
                else
            {
                MessageBox.Show("Date of birth cannot be blank");
            }
            //Cheking if second name is not blank
            if (secondname.Text.Trim().Length > 0)
            {
                //store the value
                student.SecondName1 = secondname.Text.Trim();
        }
                else
        {
            MessageBox.Show("Second name cannot be blank");
        }
            //Checking if course is not blank
            if (course.Text.Trim().Length > 0)
            {
                //store the value
                student.Course1 = course.Text.Trim();
            }
            else
            {
                MessageBox.Show("Course cannot be blank");
            }
            //Checking if level is not blank
            if (level.Text.Length > 0)
            {
                // Convert level.text to int and store it to mLevel
                int mLevel;
                bool isNumber = Int32.TryParse(level.Text.Trim(), out mLevel);
                //Display an error if isNumber is false or out of range
                if (!isNumber || mLevel < 1 || mLevel > 4) 
                {
                    MessageBox.Show("Level must be between 1 and 4.");
                }
                else
                {
                    //store the value
                    student.level = mLevel;
                }
            }
            //Cheking if First name is not blank
            if (firstname.Text.Trim().Length > 0)
            {
                //store the value
                student.FirstName1 = firstname.Text.Trim();
            }
            else
            {
                MessageBox.Show("First name cannot be blank");
            }
        }
        //Award button
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //Display the award form
            Award newWin = new Award();
            newWin.Show();
        }
        //Get button
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //updates the textboxes using the values of the student class
            firstname.Text = student.FirstName1;
            level.Text = student.level.ToString();
            course.Text = student.Course1;
            secondname.Text = student.SecondName1;
            date.Text = student.DateOfBirth1;
            credits.Text = student.Credits1.ToString();
            matric.Text = student.MatriculationNumber1.ToString();
        }
        //Clear button
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            //clears all the textboxes
            course.Text = String.Empty;
            firstname.Text = String.Empty;
            secondname.Text = String.Empty;
            date.Text = String.Empty;
            matric.Text = String.Empty;
            level.Text = String.Empty;
            credits.Text = String.Empty;
        }

        private void TextBox_TextChanged_3(object sender, TextChangedEventArgs e)
        {

        }

        
        

        }
    }

